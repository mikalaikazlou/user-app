import { Injectable } from '@nestjs/common';
import { DbProvider } from './db.provider';
import { Collection, ObjectId } from 'mongodb';
import { PostModel } from '../models/PostModel';
import { PostListFilter } from '../models/PostListFilter';
import { UserService } from './user.service';
import { pickBy } from 'lodash';

@Injectable()
export class PostService {

  private get posts(): Collection<PostModel> {
    return this.dbProvider.posts;
  }

  constructor(
    private dbProvider: DbProvider,
    private userService: UserService
  ) { }

  public getPosts({ slug, authorId }: Partial<PostListFilter> = {}): Promise<Array<Partial<PostModel>>> {
    console.log(pickBy({ slug, 'author._id': authorId }));
    return this.posts.aggregate([{
      $lookup: {
        from: 'users',
        localField: 'authorId',
        foreignField: '_id',
        as: 'author'
      },
    },
      {
        $project: {
          'author.firstName': 1,
          'author.lastName': 1,
          'author._id': 1,
          title: 1,
          created: 1,
          slug: 1,
        }
      },
      { $match: pickBy({ slug, 'author._id': authorId ? new ObjectId(authorId) : undefined }) }
    ]).toArray();
  }

  public getPostById(id: string): Promise<PostModel> {
    return this.posts.findOne(new ObjectId(id));
  }

}
