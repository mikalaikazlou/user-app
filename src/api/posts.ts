import { PostModel } from '../../server/models/PostModel';
import axios from 'axios';

const client = axios.create({
  baseURL: 'http://localhost:3001/'
});

export const getPosts = async (): Promise<Array<PostModel>> => {
  const { data } = await client.get('/post');
  return data;
};
