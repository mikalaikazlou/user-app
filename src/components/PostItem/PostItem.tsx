import React, { useMemo } from 'react';
import { PostModel } from '../../../server/models/PostModel';
import format from 'date-fns/format';
import first from 'lodash/first';

interface PostItemProps {
  item: Partial<PostModel>;
  onClick: (slug?: string) => void;
}

export const PostItem: React.FC<PostItemProps> = ({ item, onClick }) => {
  const author = useMemo(() => { return first(item?.author) }, [item]);
  return (
    <div className="card h-100">
      <div className="card-body">
        <h5 className="card-title pointer" onClick={() => onClick(item?.slug)}>{item?.title}</h5>
      </div>
      <div className="card-footer">
        {item.created && <small className="text-muted">Posted by {author?.firstName} {author?.lastName}. {format(new Date(item.created), 'PP')}</small>}
      </div>
    </div>
  )
};
